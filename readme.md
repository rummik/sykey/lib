Sykey
=========
A sync-less "telepathic" password manager that generates repeatable
site-specific passwords across any of your devices.

This package is the core library providing password generation functionality.

See the primary repository at https://gitlab.com/rummik/sykey/about for
additional information.


## Examples
```javascript
const { default: Sykey, Alphabet } = require('@sykey/lib');

console.log(new Sykey('testing').password({
  user: 'rummik',
  domain: 'rummik.com',
  length: 300,
  alphabet: Alphabet.base94,
}));
```

## Contributing
Please note that this project is released with a
[Contributor Code of Conduct][]. By participating in this project you agree to
abide by its terms.

[Contributor Code of Conduct]: ./code-of-conduct.md


## License
Copyright (c) 2012-2020 *Kim Zick (rummik)

Licensed under the MPL
